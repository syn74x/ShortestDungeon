import sys
import os
import random

player_health = 12			#Player stats
player_damage = [2, 9]
player_defense = 1
luck = 0

ogre_health = 30			#Ogre stats
ogre_damage = [1, 12]
ogre_defense = 3

goblin_health = 6			#Goblin stats
goblin_damage = [1, 4]
goblin_defense = 0

orc_health = 8				#Orc Stats
orc_damage = [1, 6]
orc_defense = 2


def dice(a, b):				#Dice funcion
	roll = random.randint(a, b)
	return roll
	
def clear_screen():			#Clear Screen function, OS specific
	if sys.platform == 'linux2' or sys.platform == 'darwin':
		os.system('clear')
	elif sys.platform == 'win32':
		os.system('cls')
	else:
		print "\n" * 5
		
def next():					#User promot to continue function
	raw_input()
	clear_screen()
	
def decide():				#User input for choices
	choice = raw_input("> ")
	clear_screen()
	return choice
	
def idiot_check(choice, user_input):	#check to see if a valid  
	if choice in user_input:			#input has been selected
		clear_screen()					#Exits the script cleanly
	else:								#rather than error out
		print "\nThat's not a valid option you shit lord."
		print "You have died due to your own stupidity.\n"
		next()
		quit("Suck a dick dumb shit")

		
def player_death():						#Exit on death
	print "\nYou have died. Sux to be you man!"
	raw_input("Press the Return key to continue ...\n ")
	quit("Fuck you")

def death_check():						#Lets see if you died
	if player_health <= 0:
		player_death()
	else:
		clear_screen()
		
def initiative():						#Generate fight initiative
	player = random.randint(1, 20)
	enemy = random.randint(1, 20)
	print "\nRolling initiative ... \n"
	next()
	print "\nYou rolled %d" % player
	if player == 20:
		print "\nAdventure!!!\n"
	else:
		print ""
	print "\nYour enemy rolled %d" % enemy
	if enemy == 20:
		print "\nAdventure!!!\n"
	else:
		print ""
	if player >= enemy:
		print "\nYou go First!\n"
		order = 1
		next()
		return order
	else:
		enemy > player
		print "\nYou go Second!\n"
		order = 2
		next()
		return order
				
def player_attacks(order, p_dmg, e_hp, e_def):		#Player attacking enemy
	damage = dice(p_dmg[0], p_dmg[1])
	damage = damage - e_def
	if damage > 0: 
		e_hp = e_hp - damage
		print "You do %d damage." % damage
		next()
		if e_hp <=0:
			print "You are Victorious!!!!"
			order = 0
			next()
		else:
			print "The fight continues."
			next()
	else: 
		damage <= 0
		print "You fail to do any damage."
		next()
	return order, e_hp
	
	
def enemy_attacks(e_dmg, p_hp, p_def):				#enemy attacking player
	damage = dice(e_dmg[0], e_dmg[1])
	damage = damage - p_def
	if damage > 0:
		p_hp = p_hp - damage
		print "You take %d damage, you have %d HP left." % (damage, p_hp)
		next()
		if p_hp <=0:
			print "You are Dead!!!!"
			next()
			player_death()
		else:
			print "The fight continues."
			next()
	else: 
		damage <= 0
		print "You you do not take any damage."
		next()
	return p_hp
	
	
def fight(p_hp, p_dmg, p_def, e_hp, e_dmg, e_def):		#String attacks together
	order = initiative()								#till something dies
	while order == 1 or order == 2:
		if order == 1:
			while order == 1:
				order, e_hp = player_attacks(order, p_dmg, e_hp, e_def)
				if e_hp > 0:
					p_hp = enemy_attacks(e_dmg, p_hp, p_def)
				else:
					order = 0
		elif order == 2:
			while order == 2:
				p_hp = enemy_attacks(e_dmg, p_hp, p_def)
				order, e_hp = player_attacks(order, p_dmg, e_hp, e_def)
		return p_hp					

#Start the script
					
clear_screen()

print "\nWelcome to the Shortest Dungeon!"
print "This is an adventure game to practice logic functions."
print "Press the Return key to begin ...\n"

next()

print """
You Have traveled for many days, across inhospitable lands.
Tales of this place are legend far and wide, riches untold
await inside ... The Shortest Dungeon!

You start with %d HP.
Your weapon is a +1 Longsword (2-9 damage).
You are wearing Leather Armour, it provides %d points of damage reduction.
""" % (player_health, player_defense)

next()

print """
An ominous mood hangs in the air as you approch the shattered ruins.
Crows argue over the bones of things long dead, oblivious to your 
presence. In front of you a small bridge leads over a muck filled ditch,
remenants of a moat from times long past.
"""

next()

print """
You stride across the bridge,confident in your abilities and try to breath only 
through your mouth. The large wooden door which used to block the 
entrace to this place offers little security, as you slip between a gap 
smashed from the centre. Evidence of adventurers, like yourself, seeking lewtz. 
"""

next()

print """
The 'Foyer' of this establishment has seen better days, of this 
you are absolutly certain. A putrid fungus appears to be growing 
from nearly every visable surface, pale and gelatinous in nature.
On the far side of the room an arched doorway looms in the shaddows.\n
"""

print "1: Walk carefuly towards the doorway."
print "2: Examine the fungus closer."
choice = decide()
user_input = ["1", "2"]

if choice == "2":
	luck = dice(1, 2)
	clear_screen()
	if luck == 1:
		print "\nAs you bend down to look closer at the oddly formed fungus"
		print "your boot slips on the slime covered stone."
		print "Your ankle folds to the side and a shooting pain rushes up your leg."
		damage = dice (1, 4)
		player_health = player_health - damage 
		print "\nYou take %d damage and have %d HP left.\n" % (damage, player_health)
		next()
		death_check()
	else:
		luck == 2
		print "\nYou see a sword, partially obscured by the mushrooms,"
		print "after wiping slime from your new find you realize this is a"
		print "blade of worth, it sings as you slice the air, the balance "
		print "and feel is unlike any other sword you have wielded."
		print "Without meta gaming you think this is +5 Longsword.\n" 
		player_damage = [6, 13]
		next()
else:
	idiot_check(choice, user_input)
	
	
print """
You enter a long hallway, dimly lit by reflections of 
light splashing off the fungus in the previous room.
You pull a torch from your pack and with a quick flick of 
your flint and steel it jumps to life.
"""

next()

print """
As the tourchlight falls into shaddows ahead of you the end of
the corridor becomes slowly visable. A chest lies at the end and 
a door to either side lead away left and right. 
As you slowly progress down the corridor you can see the partial 
remains of some poor fool, crumpled in front of the chest.\n
1: Take the left door (clockwise)
2: Take the right door (anti-clockwise)
3: Ignore the dead body and atempt to open the, clearly trapped, chest.
"""
choice = decide()
user_input = ["1", "2"]

if choice == "3":
	luck = dice(1, 4)
	print "\nYou approach the chest with caution, aware that the wrong move " 
	print "could prove deadly. You notice a tripwire connected to the "
	print "chest lid. You attempt to dissarm it ... \n"
	next()
	
	if luck == 1:
		damage = dice(1, 6)
		print "\nAs you movie closer to pin the wire a noise from deep within "
		print "the dungeon startles you, causing you to trigger the mechanism."
		print "The trap springs and several crossbow bolts fly towars you. "
		print "Fortunatly you manage to evade all but one of the bolts.\n"
		player_health = player_health - damage
		print "Tou take %d damage and have %d health remaining." % (damage, player_health)
		next()
		death_check()
	elif luck == 2:
		damage = dice(4, 24)
		print "\nAs you movie closer to pin the wire a noise from deep within "
		print "the dungeon startles you, causing you to trigger the mechanism."
		print "The trap springs and several crossbow bolts fly towars you. "
		print "Unfortunatly you take all four bolts right in the face.\n"
		player_health = player_health - damage
		print "Tou take %d damage and have %d health remaining." % (damage, player_health)
		next()
		death_check()
	elif luck == 3:
		print "\nYou disable the trap like a fucking boss!"
		print "You open the chest and find an elixir of health"
		print "This potion will provide a temporary health boost (1d8+1)."
		print "You eagerly down this bad boy.\n"
		heal = dice(2, 9)
		player_health = player_health + heal
		print "You receive %d health, your total is now %d.\n" % (heal, player_health)
		next()
	else:
		luck == 4
		print "\nYou disable the trap like a fucking boss!"
		print "You open the chest and find a suit of Elven Chain Mail!"
		print "This armour will give you a significant bonus to "
		print "damage resistance (dr5). "
		print "You don the armour ... \n"
		player_defense = 5
		next()
else:
	clear_screen()

if choice == "3":
	print "\nA choice still stands before you."
	print "1: Take the left door (clockwise)"
	print "2: Take the right door (anti-clockwise)\n"
	choice = decide()
	clear_screen()
else:
	idiot_check(choice, user_input)

if choice == "1":
	print "Going left"
	next()
	print "You encounter an angry looking orc."
	player_health = fight(player_health, player_damage, player_defense, orc_health, orc_damage, orc_defense)
	print "\nGratz!"
	print "You find an elixir of health"
	heal = dice(2, 9)
	player_health = player_health + heal
	print "You receive %d health, your total is now %d.\n" % (heal, player_health)
	next()
	print "A door sits in the right hand wall ... it is the only way to procede."
elif choice == "2":
	print "Going right"
	next()
	print "You encounter an pathetic looking goblin."
	player_health = fight(player_health, player_damage, player_defense, goblin_health, goblin_damage, goblin_defense)
	print "\nGratz!"
	print "You find an elixir of health"
	heal = dice(2, 9)
	player_health = player_health + heal
	print "You receive %d health, your total is now %d.\n" % (heal, player_health)
	next()
	print "A door sits in the left hand wall ... it is the only way to procede."
else:
	idiot_check(choice, user_input)

next()

print "\nTime for the boss fight yo!!!!"
print "At the far end of the room an Ogre growls as it senses your presence."
print "It lunges towards you, it's intent more than clear."
player_health = fight(player_health, player_damage, player_defense, ogre_health, ogre_damage, ogre_defense)

print "The Ogre slumps to the floor and he will pose no further threat "
print "as the last of his blood drains onto the floor."
print "This place and the wealth the Ogre was guarding belong to you now!"

print "\nOh shit! ... You just won"
print "What an anti-climax...\n"
next()
print "\nSorry if you feel kind of robbed, but this was really just an"
print "excercise in logic functions and nesting them inside each other.\n"
next()
quit("You Fucking Won!")
